#!/anaconda3/bin/python
import pandas as pd
import csv
import string
import argparse
import sys

def prepare_mapper(FILEPATH="data/mapper.csv"):
    """
    Parse mapper.csv as dictionary for easy reference
    Input {str}
    """
    reader = csv.reader(open(FILEPATH, 'r'))
    d = {}
    for row in reader:
        k, v = row
        if k == 'Abbreviation':
            continue
        d[k] = v
    return d


def expandAddress(abbrv_address_str, mapper):
    """
    Expand individual addresses

    Input {str} - Abbreviated addresses
    Output {str} - Manually expanded version
    """
    abbrv_address_list = abbrv_address_str.split(" ")

    def helper(word):
        word_ = word.translate(str.maketrans('', '', string.punctuation))
        if word_ in mapper:
            return mapper[word_]
        return word

    expanded_address_list = [helper(word) for word in abbrv_address_list]
    expanded_address_str = " ".join(expanded_address_list)
    print(f'"{abbrv_address_str}","{expanded_address_str}"')
    return expanded_address_str

def expandAddresses(file,mapper):
    print("Abbreviated Version","Manually Expanded Version")
    df = pd.read_csv(file)
    df['answer'] = df['Abbreviated Version'].apply(lambda x: expandAddress(x, mapper))
    return df

def main():
    mapper = prepare_mapper()
    file = sys.argv[1]
    result = expandAddresses(file, mapper)
    return result

if __name__ == "__main__":
    main()
